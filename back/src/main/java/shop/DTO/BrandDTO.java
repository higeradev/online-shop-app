package shop.DTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import shop.models.Brand;

@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class BrandDTO {

    @NotBlank
    @Size(min = 2, max = 20, message = "Length must be ≥ 2 and ≤ 20")
    private String title;

    @NotBlank
    @Size(min = 2, max = 15, message = "Length must be ≥ 2 and ≤ 15")
    private String slug;

    public Brand map() {
        return new Brand(title, slug);
    }
}
