package shop.DTO;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
public class CartDTO {
    @NotNull
    private Long productId;

    @NotNull
    private Long userId;
}
