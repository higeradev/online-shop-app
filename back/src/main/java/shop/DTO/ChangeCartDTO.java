package shop.DTO;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
public class ChangeCartDTO {

    @NotNull
    private Long productId;

    @NotNull
    private Long userId;

    @NotNull
    @Positive
    private Integer amount;
}
