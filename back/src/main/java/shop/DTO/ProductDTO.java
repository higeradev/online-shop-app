package shop.DTO;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
public class ProductDTO {

    @NotBlank
    @Size(min = 2, max = 50, message = "Length must be ≥ 2 and ≤ 50")
    private final String title;

    @NotBlank
    @Size(min = 4, max = 100, message = "Length must be ≥ 4 and ≤ 100")
    private final String description;

    @NotBlank
    private final String image;

    @NotNull
    @Positive
    private final Double price;

    @NotNull
    @PositiveOrZero
    private final Integer amount;

    @NotNull
    private final Long brand;

    @NotNull
    private final Long category;
}
