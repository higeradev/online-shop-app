package shop.DTO.users;

import lombok.*;
import shop.models.Authority;
import shop.models.User;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetUserDTO {
    private String username;
    private List<Authority> authorities;

    public static GetUserDTO map(User user) {
        return new GetUserDTO(user.getUsername(), user.getAuthorities());
    }
}
