package shop.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import shop.models.Cart;

@Configuration
public class RedisConfiguration {
    @Bean
    public RedisTemplate<String, Cart> sessionTemplate(RedisConnectionFactory factory)
    {
        RedisTemplate<String, Cart> template = new RedisTemplate<>();

        template.setConnectionFactory(factory);

        template.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
        template.setKeySerializer(new GenericJackson2JsonRedisSerializer());
        template.setHashKeySerializer(new GenericJackson2JsonRedisSerializer());
        template.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());

        return template;
    }
}
