package shop.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import shop.services.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfigurations {
    private final UserService service;
    private final PasswordEncoder encoder;

    public SecurityConfigurations(UserService service, PasswordEncoder encoder) {
        this.service = service;
        this.encoder = encoder;
    }

    @Bean
    public SecurityFilterChain chain(HttpSecurity security) throws Exception {
         security.authorizeHttpRequests(
                 (authorize) -> authorize
                                         .anyRequest().permitAll()
         )
         .formLogin(AbstractHttpConfigurer::disable)
         .logout(AbstractHttpConfigurer::disable)
         .csrf(AbstractHttpConfigurer::disable)
                 .cors(
                         (cors) -> {
                             CorsConfiguration configuration = new CorsConfiguration();
                             configuration.addAllowedHeader("*");

                             configuration.addAllowedMethod("GET");
                             configuration.addAllowedHeader("POST");
                             configuration.addAllowedHeader("PATCH");
                             configuration.addAllowedHeader("DELETE");

                             configuration.addAllowedOrigin("http://127.0.0.1:5500");

                             configuration.setAllowCredentials(true);

                             UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
                             source.registerCorsConfiguration("/**", configuration);

                             cors.configurationSource(source);
                         }
                 )
         .httpBasic(Customizer.withDefaults())
         .sessionManagement(
                 (sessions) -> {
                     sessions.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                     sessions.maximumSessions(1);
                     sessions.invalidSessionUrl("/messages/");
                     sessions.sessionFixation().migrateSession();

                 }
         );

         return security.build();
    }
    @Bean
    public DaoAuthenticationProvider provider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();

        provider.setUserDetailsService(service);
        provider.setPasswordEncoder(encoder);

        return provider;
    }

    @Bean
    public HttpSessionEventPublisher publisher() {
        return new HttpSessionEventPublisher();
    }

}
