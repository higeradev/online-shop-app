package shop.controllers;

import jakarta.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shop.DTO.CartDTO;
import shop.DTO.ChangeCartDTO;
import shop.models.Cart;
import shop.services.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {

    private final CartService service;

    public CartController(CartService service) {
        this.service = service;
    }

    @GetMapping("/{cartId}")
    public ResponseEntity<Cart> get(@PathVariable String cartId) {

        try {
            Cart cart = service.get(cartId);

            return ResponseEntity
                    .ok()
                    .body(cart);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @PostMapping
    public void setCart(@RequestBody @Valid CartDTO cartDTO) {
        service.set(cartDTO);
    }

    @PostMapping("/{cartId}")
    public void addProduct(@RequestBody @Valid CartDTO cartDTO, @PathVariable String cartId) {
        Cart cart = service.get(cartId);
        service.add(cart, cartDTO);
    }

    @PostMapping("/change/{cartId}")
    public void changeProductAmount(@RequestBody @Valid ChangeCartDTO cartDTO, @PathVariable String cartId) {
        Cart cart = service.get(cartId);
        service.changeQuantity(cart, cartDTO);
    }
}
