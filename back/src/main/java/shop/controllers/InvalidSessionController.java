package shop.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/messages")
public class InvalidSessionController {
    @GetMapping
    public String getInvalidSessionMessage() {
        return "Your session is expired!";
    }
}
