package shop.controllers;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shop.DTO.ProductDTO;
import shop.exceptions.ProductEmptyException;
import shop.services.ProductService;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @PostMapping
    public void create(@RequestBody @Valid ProductDTO productDTO) {
        service.save(productDTO);
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> get() {
        List<ProductDTO> products = service.findAll();
        if (products.isEmpty()) throw new ProductEmptyException();
        else {
            return ResponseEntity
                    .ok()
                    .body(products);
        }
    }

    @GetMapping(params = {"pageNumber", "pageSize"})
    public ResponseEntity<Page<ProductDTO>> getByPage(@RequestParam int pageNumber, @RequestParam int pageSize) {
        Pageable page = PageRequest.of(pageNumber, pageSize);
        Page<ProductDTO> products = service.getByPage(page);
        if (products.isEmpty()) throw new ProductEmptyException();
        else {
            return ResponseEntity
                    .ok()
                    .body(products);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getById(@PathVariable Long id) {
        try {
            ProductDTO productDTO = service.findById(id);

            return ResponseEntity
                    .ok()
                    .body(productDTO);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                        .notFound()
                        .build();
        }
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<List<ProductDTO>> getByCategoryId(@PathVariable Long id) {
        try {
            List<ProductDTO> products = service.findAllByCategoryId(id);

            return ResponseEntity
                    .ok()
                    .body(products);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @GetMapping("/brand/{id}")
    public ResponseEntity<List<ProductDTO>> getByBrandId(@PathVariable Long id) {
        try {
            List<ProductDTO> products = service.findAllByBrandId(id);

            return ResponseEntity
                    .ok()
                    .body(products);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @GetMapping(value = "/search", params = "name")
    public ResponseEntity<List<ProductDTO>> searchByTitle(@RequestParam("name") String title) {
        try {
            List<ProductDTO> products = service.findAllByTitleContaining(title);

            return ResponseEntity
                    .ok()
                    .body(products);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @GetMapping(value = "/search", params = "description")
    public ResponseEntity<List<ProductDTO>> searchByDescription(@RequestParam("description") String description) {
        try {
            List<ProductDTO> products = service.findAllByDescription(description);

            return ResponseEntity
                    .ok()
                    .body(products);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @ExceptionHandler(value = ProductEmptyException.class)
    public ResponseEntity<Object> exception(ProductEmptyException exception) {
        return new ResponseEntity<>("Products table is empty", HttpStatus.OK);
    }
}
