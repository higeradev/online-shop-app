package shop.controllers;

import org.springframework.web.bind.annotation.*;
import shop.DTO.users.CreateUserDTO;
import shop.DTO.users.GetUserDTO;
import shop.services.UserService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping
    public void create(@RequestBody CreateUserDTO userDTO) {
        service.create(userDTO);
    }

    @GetMapping
    public List<GetUserDTO> get() {
        return service
                    .findAll()
                    .stream()
                    .map(GetUserDTO::map)
                    .collect(Collectors.toList());
    }
}
