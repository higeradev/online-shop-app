package shop.controllers.errors;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import shop.exceptions.DownloadException;

public class DownloadErrorController {
    @ExceptionHandler(DownloadException.class)
    public ResponseEntity<?> error(DownloadException exception) {
        return new ResponseEntity<String>(exception.getMessage(), HttpStatusCode.valueOf(exception.getCode()));
    }
}
