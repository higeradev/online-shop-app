package shop.controllers.errors;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class InternalServerErrorController {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> error() {
        return new ResponseEntity<>(HttpStatusCode.valueOf(500));
    }
}
