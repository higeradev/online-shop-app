package shop.exceptions;

import lombok.*;

@Builder
@Setter @Getter
@AllArgsConstructor
public class ResourceNotFoundException extends Exception{
    private final String message;
    private final Integer code;
}
