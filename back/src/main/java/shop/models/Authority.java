package shop.models;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.time.LocalDateTime;

@Entity
@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "authorities")
public class Authority implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String authority;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    @PrePersist
    public void setCreate()
    {
        this.created = LocalDateTime.now();
    }

    @PreUpdate
    public void setUpdated()
    {
        this.updated = LocalDateTime.now();
    }
}
