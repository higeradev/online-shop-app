package shop.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brands")
public class Brand {
    public Brand(String title, String slug) {
        this.title = title;
        this.slug = slug;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime created;
    private LocalDateTime updated;
    private  Boolean isDeleted;

    private String title;
    private String slug;

    @PreUpdate
    public void setUpdated() {
        this.updated = LocalDateTime.now();
    }

    @PrePersist
    public void setCreated() {
        this.created = LocalDateTime.now();
    }
}
