package shop.models;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@RedisHash(timeToLive = 60L)
public class Cart {

    private String id;
    private List<CartWithProducts> cart;
    private Double total;
    private User user;

    public void addProduct(Product product) {
        this.cart.add(CartWithProducts.builder().product(product).amount(1).build());
        this.total += product.getPrice();
    }

    public void deleteProduct(Product product) {
        this.cart = cart.stream().filter(it -> !it.getProduct().equals(product)).collect(Collectors.toList());
    }

    public void changeProductAmount(Product product, Integer amount) {
        this.cart = cart.stream().filter(it -> !it.getProduct().equals(product)).collect(Collectors.toList());
        this.cart.add(CartWithProducts.builder().product(product).amount(amount).build());
    }

}
