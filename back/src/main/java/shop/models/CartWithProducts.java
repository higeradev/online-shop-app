package shop.models;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartWithProducts {

    private Product product;

    private Integer amount;

}
