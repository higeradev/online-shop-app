package shop.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "products")
public class Product {
    public Product(String title, String image, String description, Double price, Integer amount) {
        this.title = title;
        this.image = image;
        this.description = description;
        this.price = price;
        this.amount = amount;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    private String title;
    private String image;
    private String description;

    private Double price;
    private Integer amount;

    @ManyToOne
    private Brand brand;

    @ManyToOne
    private Category category;

    @PreUpdate
    public void setUpdated() {
        this.updated = LocalDateTime.now();
    }

    @PrePersist
    public void setCreated() {
        this.created = LocalDateTime.now();
    }
}
