package shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shop.models.Brand;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Long> {

}
