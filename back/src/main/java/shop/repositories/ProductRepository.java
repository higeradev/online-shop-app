package shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import shop.models.Brand;
import shop.models.Category;
import shop.models.Product;

import java.util.List;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long>, JpaRepository<Product, Long> {
    List<Product> findAllByCategory(Category category);
    List<Product> findAllByBrand(Brand brand);
    List<Product> findAllByTitleContainingIgnoreCase(String title);
    List<Product> findAllByDescriptionContainingIgnoreCase(String description);
}
