package shop.services;

import shop.models.Authority;

import java.util.List;

public interface AuthorityService {
    List<Authority> findAllByAuthority(String title);
}
