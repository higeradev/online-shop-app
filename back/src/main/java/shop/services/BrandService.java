package shop.services;

import shop.models.Brand;

public interface BrandService {
    void save(Brand brand);
    Brand findById(Long id); 

}
