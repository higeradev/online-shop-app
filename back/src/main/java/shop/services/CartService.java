package shop.services;

import shop.DTO.CartDTO;
import shop.DTO.ChangeCartDTO;
import shop.models.Cart;
import shop.models.Product;

public interface CartService {
    void deleteProduct(Cart cart, Product product);
    void add(Cart cart, CartDTO cartDTO);
    void changeQuantity(Cart cart, ChangeCartDTO cartDTO);
    void set(CartDTO cartDTO);
    Cart get(String cartId);

}
