package shop.services;

import shop.models.Category;

public interface CategoryService {
    void save(Category category);
    Category findById(Long id);
}
