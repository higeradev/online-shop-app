package shop.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.DTO.ProductDTO;
import shop.models.Product;

import java.util.List;

public interface ProductService {
    void save(ProductDTO productDTO);
    List<ProductDTO> findAll();
    Product findByProductId(Long id);
    ProductDTO findById(Long id);
    List<ProductDTO> findAllByCategoryId(Long categoryId);
    List<ProductDTO> findAllByBrandId(Long brandId);
    List<ProductDTO> findAllByTitleContaining(String title);
    List<ProductDTO> findAllByDescription(String description);
    Page<ProductDTO> getByPage(Pageable pageable);
}
