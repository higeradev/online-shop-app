package shop.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import shop.DTO.users.CreateUserDTO;
import shop.models.User;

import java.util.List;

public interface UserService extends UserDetailsService {
    List<User> findAll();
    void create(CreateUserDTO userDTO);
    User findByUserId(Long userId);
}
