package shop.services.impl;

import org.springframework.stereotype.Service;
import shop.models.Authority;
import shop.repositories.AuthorityRepository;
import shop.services.AuthorityService;

import java.util.List;

@Service
public class AuthorityServiceImpl implements AuthorityService {
    private final AuthorityRepository repository;

    public AuthorityServiceImpl(AuthorityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Authority> findAllByAuthority(String title) {
        return repository.findAllByAuthority(title);
    }
}
