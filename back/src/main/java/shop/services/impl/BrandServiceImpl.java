package shop.services.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.models.Brand;
import shop.repositories.BrandRepository;
import shop.services.BrandService;

import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;

    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }


    @Override
    public void save(Brand brand) {
        brandRepository.save(brand);
    }

    @Override
    public Brand findById(Long id) {
        Optional<Brand> brand = brandRepository.findById(id);
        if (brand.isPresent()) {
            return brand.get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Brand with such ID doesn't exist!");
    }
}
