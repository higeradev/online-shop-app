package shop.services.impl;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import shop.DTO.CartDTO;
import shop.DTO.ChangeCartDTO;
import shop.models.Cart;
import shop.models.CartWithProducts;
import shop.models.Product;
import shop.models.User;
import shop.services.CartService;
import shop.services.ProductService;
import shop.services.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class CartServiceImpl implements CartService {
    private final RedisTemplate<String, Cart> redis;
    private final UserService userService;
    private final ProductService productService;

    public CartServiceImpl(RedisTemplate<String, Cart> redis, UserService userService, ProductService productService) {
        this.redis = redis;
        this.userService = userService;
        this.productService = productService;
    }

    @Override
    public void deleteProduct(Cart cart, Product product) {
        cart.deleteProduct(product);
    }

    @Override
    public void add(Cart cart, CartDTO cartDTO) {
        Product product = productService.findByProductId(cartDTO.getProductId());
        User user = userService.findByUserId(cartDTO.getUserId());
        if (cart != null) {
            cart.addProduct(product);
        }
        else {
            Cart newCart = Cart.builder().cart(new ArrayList<>()).user(user).total((double) 0).build();
            newCart.addProduct(product);
        }
    }

    @Override
    public void changeQuantity(Cart cart, ChangeCartDTO cartDTO) {
        Product product = productService.findByProductId(cartDTO.getProductId());
        cart.changeProductAmount(product, cartDTO.getAmount());
    }

    @Override
    public void set(CartDTO cartDTO) {
        String id = UUID.randomUUID().toString();

        Product product = productService.findByProductId(cartDTO.getProductId());
        User user = userService.findByUserId(cartDTO.getUserId());
        Cart newCart = Cart
                            .builder()
                            .cart(new ArrayList<>())
                            .user(user)
                            .total((double) 0)
                            .build();

        newCart.addProduct(product);

        redis.opsForValue().set(
                "cart:" + id,
                newCart
                );

        redis.expire("sessions:" + id, 60L, TimeUnit.DAYS);
    }

    @Override
    public Cart get(String cartId) {
        return redis.opsForValue().get("cart:" + cartId);
    }
}
