package shop.services.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.DTO.ProductDTO;
import shop.models.Brand;
import shop.models.Category;
import shop.models.Product;
import shop.repositories.ProductRepository;
import shop.services.BrandService;
import shop.services.CategoryService;
import shop.services.ProductService;
import shop.utils.DTOConverter;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final BrandService brandService;
    private final CategoryService categoryService;

    public ProductServiceImpl(ProductRepository productRepository, BrandService brandService, CategoryService categoryService) {
        this.productRepository = productRepository;
        this.brandService = brandService;
        this.categoryService = categoryService;
    }

    @Override
    public void save(ProductDTO productDTO) {
        Brand brand = brandService.findById(productDTO.getBrand());
        Category category = categoryService.findById(productDTO.getCategory());
        Product product = Product.builder()
                                        .brand(brand)
                                        .category(category)
                                        .image(productDTO.getImage())
                                        .title(productDTO.getTitle())
                                        .amount(productDTO.getAmount())
                                        .price(productDTO.getPrice())
                                        .description(productDTO.getDescription())
                                        .build();

        productRepository.save(product);
    }

    @Override
    public List<ProductDTO> findAll() {
        List<Product> products = productRepository.findAll();
        return products.stream().map(DTOConverter::from).toList();
    }

    @Override
    public Product findByProductId(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            return product.get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product with such ID doesn't exist!");
    }

    @Override
    public ProductDTO findById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            Product product1 = product.get();
            Long brandId = product1.getBrand().getId();
            Long categoryId = product1.getCategory().getId();
            return ProductDTO.builder()
                                                    .title(product1.getTitle())
                                                    .description(product1.getDescription())
                                                    .image(product1.getImage())
                                                    .price(product1.getPrice())
                                                    .amount(product1.getAmount())
                                                    .brand(brandId)
                                                    .category(categoryId)
                                                    .build();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product with such ID doesn't exist!");
    }

    @Override
    public List<ProductDTO> findAllByCategoryId(Long categoryId) {
        Category category = categoryService.findById(categoryId);
        List<Product> products = productRepository.findAllByCategory(category);
        return products.stream().map(DTOConverter::from).toList();
    }

    @Override
    public List<ProductDTO> findAllByBrandId(Long brandId) {
        Brand brand = brandService.findById(brandId);
        List<Product> products = productRepository.findAllByBrand(brand);
        return products.stream().map(DTOConverter::from).toList();
    }

    @Override
    public List<ProductDTO> findAllByTitleContaining(String title) {
        List<Product> products = productRepository.findAllByTitleContainingIgnoreCase(title);
        return products.stream().map(DTOConverter::from).toList();
    }

    @Override
    public List<ProductDTO> findAllByDescription(String description) {
        List<Product> products = productRepository.findAllByDescriptionContainingIgnoreCase(description);
        return products.stream().map(DTOConverter::from).toList();
    }

    @Override
    public Page<ProductDTO> getByPage(Pageable pageable) {
        Page<Product> page = productRepository.findAll(pageable);
        return page.map(DTOConverter::from);
    }

}
