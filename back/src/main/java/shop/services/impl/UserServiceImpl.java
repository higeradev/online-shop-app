package shop.services.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.DTO.users.CreateUserDTO;
import shop.models.User;
import shop.repositories.UserRepository;
import shop.services.AuthorityService;
import shop.services.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final PasswordEncoder encoder;
    private final AuthorityService service;
    private final UserRepository repository;

    public UserServiceImpl(PasswordEncoder encoder, UserRepository repository, AuthorityService service) {
        this.encoder = encoder;
        this.repository = repository;
        this.service = service;
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public void create(CreateUserDTO userDTO) {
        User user = User
                        .builder()
                        .username(userDTO.getUsername())
                        .password(encoder.encode(userDTO.getPassword()))
                        .authorities(service.findAllByAuthority(userDTO.getAuthority()))
                        .build();

        repository.save(user);
    }

    @Override
    public User findByUserId(Long userId) {
        Optional<User> user = repository.findById(userId);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User with username %s doesn't exist!".formatted(userId));
        }

        return user.get();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = repository.findByUsername(username);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User with username %s doesn't exist!".formatted(username));
        }

        return user.get();
    }
}
