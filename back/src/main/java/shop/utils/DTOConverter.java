package shop.utils;

import shop.DTO.ProductDTO;
import shop.models.Product;

public class DTOConverter {
    public static ProductDTO from(Product product) {
        return ProductDTO.builder()
                .title(product.getTitle())
                .description(product.getDescription())
                .image(product.getImage())
                .price(product.getPrice())
                .amount(product.getAmount())
                .brand(product.getBrand().getId())
                .category(product.getCategory().getId())
                .build();

    }
}
