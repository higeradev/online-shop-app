CREATE TABLE products
(
    "id"                  SERIAL                 NOT NULL,
    "created"             TIMESTAMP              NOT NULL,
    "updated"             TIMESTAMP              DEFAULT NULL,
    "is_deleted"          BOOLEAN                DEFAULT  FALSE,

    "title"               VARCHAR(50)            NOT NULL,
    "image"               VARCHAR                DEFAULT NULL,
    "description"         VARCHAR(100)           NOT NULL,
    "price"               NUMERIC                NOT NULL,
    "amount"              INTEGER                NOT NULL,
    "brand_id"            INTEGER                NOT NULL,
    "category_id"         INTEGER                NOT NULL,

    CONSTRAINT products__pkey
        PRIMARY KEY (id),

    CONSTRAINT products_brands__fk
        FOREIGN KEY (brand_id)
            REFERENCES brands (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT products_categories__fk
        FOREIGN KEY (category_id)
            REFERENCES categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

INSERT INTO products (created, title, description, price, amount, brand_id, category_id) VALUES
(CURRENT_DATE, 'Apple iPhone 11 128Gb', 'Apple iPhone 11 128Gb Slim Box черный', 300, 3, 1, 1),
(CURRENT_DATE, 'Samsung Galaxy A24', 'Samsung Galaxy A24 6 ГБ/128 ГБ черный', 200, 5, 2, 1),
(CURRENT_DATE, 'Apple AirPods 3', 'Apple AirPods 3 with Lightning Charging Case белый', 100, 10, 1, 2),
(CURRENT_DATE, 'Наушники Sony WH-1000XM4', 'Наушники Sony WH-1000XM4 черный', 120, 3, 3, 2),
(CURRENT_DATE, 'Apple iPad Air 2022', 'Apple iPad Air 2022 Wi-Fi 10.9 дюйм 8 Гб/256 Гб серый', 290, 7, 1, 3);


