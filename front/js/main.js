const productsContainer = document.getElementById("products");

const getProducts = (products) => {
  for (let product of products) {
    const newProduct = document.createElement("div");
    newProduct.classList.add("event");
    newProduct.innerHTML = product.title;
    productsContainer.append(newProduct);
  }
}

axios.get("http://localhost:8080/api/v1/products",
{
  auth:
  {
    username: "alisa",
    password: "testpw!!"
  }
}).then(response => getProducts(response.data));

document.getElementById("form").addEventListener("submit", (it) => {
  it.preventDefault()

  axios.post("http://localhost:8080/api/v1/users",
  {
      username: document.getElementById("username").value,
      password: document.getElementById("password").value
  }).then(response => console.log(response.data));
})

document.getElementById("login").addEventListener("submit", (it) => {
  it.preventDefault()

  axios.post("http://localhost:8080/api/v1/login?username=" + document.getElementById("username_login").value + "&password=" + document.getElementById("password_login").value,
      ).then(response => {
          console.log(response)
          console.log(response.headers)
          console.log(response.headers[('access_token')])

          localStorage.setItem('access_token', response.headers[('access_token')]);
          localStorage.setItem('refresh_token', response.headers[('refresh_token')]);
      });
})